package course.labs.activitylab;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class ActivityOne extends Activity {

		// string for logcat documentation
		private final static String TAG = "Lab-ActivityOne";

		// lifecycle counts
		int onCreateCount = 0,
				onStartCount = 0,
				onResumeCount = 0,
				onPauseCount = 0,
				onStopCount = 0,
				onRestartCount = 0,
				onDestroyCount = 0;
		
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_one);
			
			//Log cat print out
			Log.i(TAG, "onCreate called");

			onCreateCount++;
			displayContent();
		}

		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.activity_one, menu);
			return true;
		}
		
		// lifecycle callback overrides
		
		@Override
		public void onStart(){
			super.onStart();
			
			//Log cat print out
			Log.i(TAG, "onStart called");

			onStartCount++;
			displayContent();
		}

		@Override
		public void onResume() {
			super.onResume();

			Log.i(TAG, "onResume called");

			onResumeCount++;
			displayContent();
		}

		@Override
		public void onPause() {
			super.onPause();

			Log.i(TAG, "onPause called");

			onPauseCount++;
			displayContent();
		}

		@Override
		public void onStop() {
			super.onStop();

			Log.i(TAG, "onStop called");

			onStopCount++;
			displayContent();
		}

		@Override
		public void onRestart() {
			super.onRestart();

			Log.i(TAG, "onRestart called");

			onRestartCount++;
			displayContent();
		}

		@Override
		public void onDestroy() {
			super.onDestroy();

			Log.i(TAG, "onDestroy called");

			onDestroyCount++;
			displayContent();
		}

		@Override
		public void onSaveInstanceState(Bundle savedInstanceState){
			savedInstanceState.putInt("onCreate", onCreateCount);
			savedInstanceState.putInt("onStart", onStartCount);
			savedInstanceState.putInt("onResume", onResumeCount);
			savedInstanceState.putInt("onPause", onPauseCount);
			savedInstanceState.putInt("onStop", onStopCount);
			savedInstanceState.putInt("onRestart", onRestartCount);
			savedInstanceState.putInt("onDestroy", onDestroyCount);
		}

		@Override
		public void onRestoreInstanceState(Bundle saveInstanceState) {
			super.onRestoreInstanceState(saveInstanceState);

			onCreateCount = saveInstanceState.getInt("onCreate");
			onStartCount = saveInstanceState.getInt("onStart");
			onResumeCount = saveInstanceState.getInt("onResume");
			onPauseCount = saveInstanceState.getInt("onPause");
			onStopCount = saveInstanceState.getInt("onStop");
			onRestartCount = saveInstanceState.getInt("onRestart");
			onDestroyCount = saveInstanceState.getInt("onDestroy");
		}


		public void launchActivityTwo(View view) {
			startActivity(new Intent(this, ActivityTwo.class));
		}

		public void displayContent() {
			((TextView) findViewById(R.id.create)).setText(getResources().getString(R.string.onCreate) + Integer.toString(onCreateCount));
			((TextView) findViewById(R.id.start)).setText(getResources().getString(R.string.onStart) + Integer.toString(onStartCount));
			((TextView) findViewById(R.id.resume)).setText(getResources().getString(R.string.onResume) + Integer.toString(onResumeCount));
			((TextView) findViewById(R.id.pause)).setText(getResources().getString(R.string.onPause) + Integer.toString(onPauseCount));
			((TextView) findViewById(R.id.stop)).setText(getResources().getString(R.string.onStop) + Integer.toString(onStopCount));
			((TextView) findViewById(R.id.restart)).setText(getResources().getString(R.string.onRestart) + Integer.toString(onRestartCount));
			((TextView) findViewById(R.id.destroy)).setText(getResources().getString(R.string.onDestroy) + Integer.toString(onDestroyCount));
		}
}
